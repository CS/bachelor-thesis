\chapter{Konzept}
Dieses Kapitel beschreibt welche Simulation verwendet wird und erklärt den Prozess der Modellentwicklung in Uppaal.
Dann folgt eine Erläuterung der Funktionsweise des implementierten Adapters sowie der Verwendung dessen.

\section{Simulation}\label{konzept:simulation}
Als Simulator bietet sich Gazebo an, da es sich einfach in ROS integrieren lässt und eine relativ hohe physikalische Korrektheit bietet. Verschiedene Physik-Engines können innerhalb von Gazebo verwendet werden.
Auch sind Plug-Ins unterstützt, sodass sich auch nicht nativ implementiere Sachverhalte wie etwa die Umgebungstemperatur darstellen lassen \cite{Harris2011}. Gazebo ist daher und aufgrund seiner Stabilität in der Robotik und insbesondere in der Forschung die momentan am meisten verwendete Simulationssoftware.
Allerdings können komplexe Zusammenhänge, auch abhängig von der gewünschten Akkuratheit, bei einer Implementierung in Form von Plug-Ins den Entwicklungsaufwand stark erhöhen. Problematisch ist bei Gazebo die Performance bei vielen simulierten Robotern oder komplexen Umgebungen, sodass in diesen Fällen potenziell andere Simulatoren verwendet werden sollten \cite{Ivaldi2014}.
\section{Uppaal-Modellierung}
Vor dem Erstellen eines Modells müssen zunächst die gewünschten Anforderungen des Systems formal festgehalten werden. Diese formalen Anforderungen sind dann zu quantisieren. Dabei bieten sich besonders für die nicht-funktionalen Eigenschaften Invarianten im Modell an.
Da für die Testgenerierung auch ein Modell für die Inputs des Systems nötig ist, kann das Modellverhalten auch innerhalb von Uppaal verifiziert werden. Ebenso lassen sich formale Eigenschaften mithilfe des integrierten Verifiers überprüfen. Wie in \cref{grundlagen:testentwicklung:modell-basiert} erwähnt können, falls schon eine Implementierung vorhanden ist, auch Modelle die zur Entwicklung verwendet wurden zur Verifizierung beitragen.

\section{TRON-Adapter}
Um den Adapter an da ROS-System anzuknüpfen, wurde eine Adapter-Klasse geschrieben, welche die Pakete des Adapters annimmt und intern über die ROS Topics an das System weitergibt. Diese Übersetzung ist generisch implementiert, sodass zukünftig für ROS-Projekte dieser Adapter bis auf die Konfiguration wiederverwendet werden kann. Um die Umgebung in der Simulation auch für TRON verfügbar zu machen, müssen die dafür vorhandenen Daten über ein ROS Topic im System veröffentlicht und dann durch den Adapter weitergereicht werden. 
\cref{konzept:bild} zeigt den schematischen Aufbau der Implementierung.
\begin{figure}[H]
	\centering
	\includegraphics[scale=.5]{./konzept.png}
	\caption{Aufbau der Testmethode}
	\label{konzept:bild}
\end{figure}
Im Leitfaden zu TRON~\cite{TronManual} sind die verwendeten Pakete beschrieben und werden im Folgenden in den Kontext eingeordnet und erläutert wie diese verwendet werden.

\subsection{Adapter-Phasen}
\subsubsection{Konfigurierungsphase}
Zunächst wird der SocketAdapter auf eine eingehende Verbindung warten. Folglich werden Konfigurationsdaten ausgetauscht. Strings sind dabei durch ein Byte mit der Länge gefolgt von dieser Anzahl an Bytes repräsentiert. Als Erstes werden die verschiedenen Channel als Input beziehungsweise Output deklariert. Dazu wird ein Paket mit dem ersten Byte 1 beziehungsweise 2 und dem Namen des jeweiligen Channels im Uppaal Modell versendet. Als Antwort empfängt der Node einen 32-Bit Integer, der die ab diesem Zeitpunkt für diesen Channel genutzte ID widerspiegelt. Ist dieser kleiner oder gleich 0, so trat ein Fehler auf. Die Fehlermeldung kann dann mittels eines Pakets ermittelt werden (erstes Byte 127, dann Fehlercode). Nach Erhalten der Channel-IDs werden diese genutzt, um die Namen der Variablen im Uppaal Modell dem jeweiligen Channel zuzuordnen. Als letzter Schritt der Konfigurierungsphase werden mittels der Codes 5 und 6 die Zeiteinheiten sowie der Timeout zum Beenden des Tests gesetzt. \cref{konzept:tron_konfigurierung} skizziert den fehlerfreien Ablauf dieser Phase.  Für Output-Channel müssen die dargestellten Bytecodes 1 und 3 durch 2 und 4 ersetzt werden.
\begin{figure}[h]
	\centering
	\includegraphics[scale=.6]{./tron_konfigurierung.png}
	\caption{Fehlerfreier Ablauf der Konfigurierung von TRON}
	\label{konzept:tron_konfigurierung}
\end{figure}
\subsubsection{Ausführungsphase}
Nach erfolgreicher Konfiguration sendet der Node ein Byte mit dem Wert 64, um den Start des Tests anzufragen, als Antwort erhält er ein Byte mit dem Wert 0. Folglich werden Inputs und Outputs ausgetauscht, dabei bestehen die ersten 4 Byte der Nachrichten aus einem Channel-Identifier, worauf 2 Byte mit der Anzahl an verknüpften Variablen und schließlich die Variablen (je 4 Byte) folgen. Diese müssen dann innerhalb der ROS-Topics verbreitet werden. Als Antwort wird von beiden Seiten eine Bestätigung zurückgesendet, Inputs und Outputs können jedoch asynchron übertragen werden.

\subsection{Implementierung}
Der implementierte Adapter verwendet vom Betriebssystem bereitgestellte Sockets für die Kommunikation mit TRON und hält relevante Variablen, in welchen die Abbildungen von Channel auf Topics sowie die zugehörigen Variablen beschrieben werden, innerhalb einer Klasse. Die Verwendung dieser wird im nächsten Abschnitt dargestellt.
Bei der Implementierung des Adapters (siehe \cref{header}) sind einige Unklarheiten im Handbuch zu TRON (\cite{TronManual}) aufgefallen. So werden die Bytecodes 64 (tatsächlich zum Starten verwendet) und 127 (verwendet zum Anfragen von Fehlermeldungen) vertauscht. Lokale Variablen sowie probabilistische Kanten wie in \cref{konzept:uppaal_tor} werden von TRON nicht unterstützt, im Handbuch allerdings auch nicht erwähnt. Außerdem ist bei der Übertragung der Zeit pro Zeiteinheit die Rede von zwei 32-Bit-Integer-Variablen, während TRON tatsächlich einen 64-Bit-Integer (ohne Vorzeichen) verwendet, um die Anzahl an Mikrosekunden einer Modell-Zeiteinheit festzulegen. Derartige Fehlinformationen sind vermutlich darauf zurückzuführen, dass Uppaal sowie TRON selbst Updates erhielten, ohne dass das Handbuch angepasst wurde.\\
Der Adapter wurde möglichst generisch gehalten und ist somit für sämtliche Nachrichten die innerhalb von ROS Topics ausgetauscht werden verwendbar. Einem Channel des Modells lassen sich beliebig viele Topics zuweisen und umgekehrt. 
Templates ermöglichen generische Callbacks, mit denen den Uppaal-Variablen Byte-Positionen innerhalb der ROS-Nachrichten zugewiesen werden, es sind jedoch auch eigene Implementierungen zum Beispiel für Felder mit variabler Länge möglich, welche in den meisten Fällen zu empfehlen sind.
\subsubsection{Verwendung}
Zur Verwendung des Adapters muss eine Instanz der Klasse \textit{TRON\_Adapter} erstellt werden. Ihr wird die Ziel-IP-Adresse sowie der Port übergeben, über den eine bereits laufende TRON Instanz erreichbar ist. Die Struktur \textit{Mapping} beschreibt das Verhältnis von einem Channel zu einem Topic und muss daher für jedes gewünschte Paar mittels der Funktion \textit{createMapping} initialisiert werden. Dabei ist (nach dem Namen des Topics und dem Namen des Channels) anzugeben, ob dieses Mapping als Eingabe oder Ausgabe dient. Soll ein Channel sowohl Eingabe als auch Ausgabe sein, oder sollen mehrere Topics einem Channel zugewiesen werden (oder umgekehrt), so müssen dafür zusätzliche Mappings erstellt werden.
\\ Nach der Initialisierung lassen sich Variablen hinzufügen. Dazu wird dem Adapter über \textit{add\_var\_to\_mapping} neben dem Mapping selbst der Name der Variablen in Uppaal übergeben. Optionale Parameter wie zum Beispiel der Offset können angegeben werden, wenn die Übersetzung mittels Byte-Positionen innerhalb der ROS-Nachricht stattfinden soll. Der Offset beginnt vom Ende des letzten Feldes, sodass in diesem Fall die Variablen in der richtigen Reihenfolge angegeben werden müssen.
\\ Für Input-Channel ist zusätzlich ein Callback zu spezifizieren, welcher als Parameter eine Referenz zum entsprechenden Mapping und ein Array von 32-Bit-Integern, den von TRON übergebenen Variablen, bekommt. \textit{mapping\_callback\_to\_topic} kann dafür verwendet werden, um mit den festgelegten Byte-Positionen zu arbeiten. Output-Channel haben keinen separaten Callback, da dieser beim Abonnieren eines Topics durch die ROS API festgelegt wird. Wie bei den Input-Channel ist dafür eine generische Funktion vorhanden, \textit{mappings\_callback\_to\_TRON}, welche bei einfachen Nachrichten verwendet werden kann. Anzumerken ist hierbei, dass diese Methode alle Channel informiert, die als Output mit dem Topic angegeben sind, während bei den Inputs jeweils eine Funktion im Mapping hinterlegt ist.
\\ Schließlich muss das Mapping der Liste \textit{mappings} hinzugefügt werden. Zu beachten ist, dass für jedes Topic, welches im Rahmen von Input-Mappings verwendet wird ein entsprechender \textit{ros::Publisher} in der Liste \textit{input\_publishers} vorhanden sein muss. Analog dazu müssen \textit{ros::Subscriber} zu \textit{output\_subscriber} hinzugefügt werden.
\\ Neben den Mappings muss auch die Zeiteinheit sowie der Zeitraum für den getestet werden soll festgelegt werden, dies geschieht über einen Funktionsaufruf an \textit{set\_time\_unit\_and\_timeout}.
\\ Für das in \cref{konzept:uppaal_tor} dargestellte Modell könnte die Konfigurierung etwa so aussehen:
\begin{lstlisting}[tabsize=2]
// Input Channel "ausloesen" wird auf Topic "/command" abgebildet
// es werden keine Variablen uebertragen
Mapping map = adapter.createMapping("/command", "ausloesen", true);
map.input_callback = boost::bind(&TRON_Adapter::mapping_callback_to_topic<std_msgs::Empty>, &adapter, _1, _2);
adapter.input_publishers.push_back(nh.advertise<std_msgs::Empty>("/command", 10));
adapter.mappings.push_back(map);

// Output Channel "position" wird auf Topic "/position" abgebildet
map = adapter.createMapping("/position", "position", false);
// es wird eine Variable namens "pos" uebertragen, die sich bei Offset 0 (direkt am Anfang der Nachricht) befindet
adapter.add_var_to_mapping(map, "pos", 0);
adapter.mappings.push_back(map);
adapter.output_subscribers.push_back(
		nh.subscribe<std_msgs::Int32>("/position", 10, 
		// Callback als letztes Argument von ros::Nodehandle::subscribe
		boost::function<void(const ros::MessageEvent<std_msgs::Int32>&)>(
		boost::bind(&TRON_Adapter::mappings_callback_to_TRON<std_msgs::Int32>, &adapter, _1))));

// 1000 Mikrosekunden pro Uppaal Zeiteinheit
// 100000 Zeiteinheiten lang testen
adapter.set_time_unit_and_timeout(1000, 100000);
\end{lstlisting}
Der Channel \textit{position} sowie die Variable \textit{pos} sind in \cref{konzept:uppaal_tor} nicht dargestellt, werden hier aber verwendet, um einen Output-Channel sowie das Übertragen einer Variable zu demonstrieren.
Ein besonderes Augenmerk liegt auf dem Callback des ros::Subscriber. Dieser benötigt nach dem boost::bind der Adapter-Instanz an die Funktion einen expliziten Cast zu einer boost::function des entsprechenden Typs, da der Compiler diesen nicht zuverlässig selbstständig ableiten kann und dennoch keinen Fehler ausgibt. Dies ist auf der zum Thema passenden Übersichtsseite von ROS\footnote{\url{http://wiki.ros.org/roscpp/Overview/Publishers\%20and\%20Subscribers}} in einer Notiz unter 2.3.3 angemerkt.
\\
In den meisten praktischen Fällen ist das Verwenden eigens für bestimmte Nachrichten implementierter Callbacks sinnvoller, da ROS-Nachrichten sehr komplex sein können und viele Felder variabler Größe verwendet werden, wofür Konvertierungsfunktionen nötig wären. Außerdem wird dadurch die Lesbarkeit des Codes erhöht und folglich die Wartung erleichtert. Benutzerdefinierte Callbacks können \textit{report\_now} und \textit{publish\_to\_topic} nutzen um Nachrichten zu TRON beziehungsweise zu den Topics zu senden.
Auch hier ist zu erwähnen, dass die Übersetzung von einem Topic zu mehreren Channels innerhalb lediglich einer Methode stattfinden kann, die dem ROS-Subscriber als Parameter übergeben werden muss.
Diese Variante wird auch bei einer später im Text folgenden beispielhaften Anwendung des Adapters für die sogenannte \textit{actionlib} sowie im durchgeführten Testbeispiel (siehe \cref{fallbeispiel}) verwendet.
