\chapter{Fallbeispiel}\label{fallbeispiel}
In diesem Kapitel wird zunächst die Möglichkeit der Integration des Testadapters in ein System, welches die actionlib verwendet, beispielhaft überprüft.
Die actionlib \cite{Actionlib} baut ein Protokoll auf den Nachrichten von ROS auf und ermöglicht Kommunikation von Nodes für das Ausführen einer bestimmten Aufgabe mithilfe einer Client-Server-Architektur. Diese unterscheidet sich von den \textit{ROS Services}\footnote{\url{http://wiki.ros.org/Services}} insbesondere durch die ermöglichte Asynchronität, welche für konstante Rückmeldung des Servers sowie weitere Anfragen des Clients (wie etwa nach der Präemptierung einer Aktion) genutzt wird. 
Dazu wurde von der Bibliothek die Implementierung eines endlichen Automaten realisiert, was sich neben den verwendeten ROS-Nachrichten für das Testen mittels TRON aufgrund der einfachen Modellierung in Uppaal anbietet. \\
Wenn die Integration möglich ist, wird folgend das am Lehrstuhl verwendete Cobot-Projekt\footnote{\url{https://git-st.inf.tu-dresden.de/ceti/ros/chemistry-lab-demo/}} (verwendete Version \href{https://git-st.inf.tu-dresden.de/ceti/ros-internal/chemistry-lab-demo/cobot_1/-/tree/0ab6cd6d48613f4cb913e433832ccd741682bcde}{0ab6cd6d}) benutzt, um den Testvorgang mittels des vorgestellten Konzeptes zu evaluieren. Der Roboter (Franka Panda\footnote{\url{https://www.franka.de/robot-system}}) soll eine Flasche aufheben, um damit ein Glas zu befüllen. Folglich wird die Flasche wieder an ihre Anfangsposition gebracht und das nun befüllte Glas auf die andere Seite des Roboters transportiert. Der Ablauf ist in \cref{fallbeispiel:cobot} dargestellt.

\begin{figure}[H]
	\centering
	\includegraphics[scale=.4]{./cobot_ablauf.png}
	\caption{Ablauf der Aktionen des Cobots, aus \cite{Molkenthin2021}}
	\label{fallbeispiel:cobot}
\end{figure}

\section{Integration mit actionlib}\label{actionlib:integration}
Hier wird überprüft, ob der Testadapter bei Systemen, welche auf der actioblib basieren, verwendbar ist.
Das hier verwendete Szenario entspricht einer etwas erweiterten Version des in \cref{konzept:uppaal_tor} verwendeten Beispiels, einem Garagen-Tor, welches präemptiert werden kann. Der implementierte Action-Server nimmt einen Boolean entgegen, der festlegt, ob das Tor geöffnet oder geschlossen werden soll, und gibt dann zufällig Feedback in Form eines Integers zurück, welcher die aktuelle Position des Tores repräsentiert. Dabei steht der Wert 0 für ein geschlossenes sowie 1000 für ein offenes Tor. Nach Abschluss des aktuellen Vorgangs wird der Status zurück zum Client übertragen. Dieser kann jedoch auch während der Ausführung das Wechseln des Ziels anfragen, woraufhin der Server das alte Ziel verwirft.\\
Das entsprechend erstellte Uppaal-Modell ist in \cref{integration:uppaal} zu sehen. Auffällig dabei sind Kanten denen die Channel \textit{fertig} und \textit{position} zugewiesen sind. Bei diesen wird vom System die aktuelle Position übermittelt. Dazu wird eine zufällige Zahl zwischen 0 und 1000 ausgewählt und zur Position addiert beziehungsweise subtrahiert. Außerdem wird garantiert, dass die Grenzen des Wertebereiches nicht überschritten werden.
\begin{figure}[h]
	\centering
	\includegraphics[scale=.2]{./actionlib_garage.png}
	\caption{Testmodell des Action-Servers}
	\label{integration:uppaal}
\end{figure}
Der Einsatz des Adapters erfolgt unter Verwendung von eigens für die Nachrichtentypen implementierten Callbacks statt der Zuweisung von Bytes. Dadurch können andere Bestandteile des Action-Protokolls wie die Header vernachlässigt werden. Weiterhin ist zu beachten, dass im Adapter die Nachrichten verwendet werden, die \textbf{Action} im Namen tragen (z.B. TriggerActionGoal, nicht TriggerGoal). Diese Nachrichten beinhalten neben den Daten auch Header- und Statusinformationen und müssen verwendet werden, wenn der Adapter selbst weder Client noch Server des Systems ist, sondern ersteren nur emuliert oder die Kommunikation beider Parteien abhört. Eine Implementierung des Adapters, welcher selbst einen Client darstellt, wäre allerdings auch möglich. Die Namen der Topics sind aus dem Namen des Servers (wird bei Konstruktion übergeben) und dem Inhalt der Nachricht zusammengesetzt und können daher ohne Probleme verwendet werden. Die (nicht vollständige) Konfigurationsphase für den Adapter des in \cref{integration:uppaal} gezeigten Modells sieht wie folgt aus:
\\
\begin{lstlisting}[tabsize=1]
	// oeffnen und schliessen stellt jeweils ein Ziel dar
	Mapping map = adapter.createMapping("/garage_server/goal", "oeffnen", true);
	map.input_callback = send_goal_open;
	adapter.mappings.push_back(map);
	
	map = adapter.createMapping("/garage_server/goal", "schliessen", true);
	map.input_callback = send_goal_close;
	adapter.mappings.push_back(map);
	
	// wichtig ist, dass die Queue ausreichend gross ist, falls TRON
	// viele Inputs sendet (hier relativ unwichtig)
	adapter.input_publishers.push_back(nh.advertise<actionlib_example::TriggerActionGoal>("/garage_server/goal", 100));
	
	// aktuelle Position wird per feedback zureckgegeben
	// (wie bei result-Topic an Channel 'fertig')
	map = adapter.createMapping("/garage_server/feedback", "position", false);
	adapter.add_var_to_mapping(map, "akt_position");
	adapter.mappings.push_back(map);
	adapter.output_subscribers.push_back(nh.subscribe("/garage_server/feedback", 100, 
	feedback_callback));
\end{lstlisting}
Die hier verwendeten Callbacks \textit{send\_goal\_open(/close)} rufen die Funktion \textit{send\_goal} auf:
\begin{lstlisting}[tabsize=1]
	void send_goal (Mapping& map, int32_t* val, bool open){
		auto shared_ptr = boost::make_shared<actionlib_example::TriggerActionGoal>();
		shared_ptr->goal.open = open;
		publish_to_topic<actionlib_example::TriggerActionGoal>(map.topic, shared_ptr);
	}
\end{lstlisting}
Es lässt sich hinzufügen, dass beim Versenden eines Ziels (oder einer sonstigen Nachricht) vom (hier emulierten) Client keine Felder im Header wie zum Beispiel die Identifikationsnummer dieses Zieles gesetzt werden müssen, da der Server diese beim Empfangen der Nachricht entsprechend ausfüllt, sollten sie leer sein. Die Identifikationsnummer ist für den Client wichtig, um einen bestimmten Auftrag abzubrechen und sollte folglich in diesem Fall von ihm gesetzt werden. Die Funktion \textit{feedback\_callback} gibt lediglich die aktuelle Position an TRON weiter. Da im Uppaal-Modell keine Angaben zur Zeit vorhanden sind, kann die Zeiteinheit in diesem Fall frei gewählt werden. Der Test-Timeout sollte so gewählt sein, dass mehrere Zyklen durchlaufen werden können und ist daher abhängig von der Zeit, die der Server zum Bewältigen der Aufgabe benötigt.
Die vollständige Implementierung des actionlib-Servers und -Clients sowie des verwendeten TRON-Adapters sind in einem Git-Repository vorhanden (siehe \cref{git-repos}).

\section{Cobot}
Mit der Möglichkeit zur Verwendung der actionlib kann nun ein tatsächliches Testszenario angegangen werden. Dazu wird zunächst die Aufgabe des Systems überprüft und formale Bedingungen zur Ausführung festgehalten. Dabei wurde der Cobot mehrfach simuliert sowie der Code der Implementierung überflogen, um einen Eindruck vom Systemverhalten zu bekommen.
\subsection{Formales Modell}\label{cobot:formales_modell}
Als Erstes lässt sich feststellen, dass der Cobot, nachdem er seine Startposition angenommen hat, wartet bis er ein Signal von einem Drucksensor erhält, woraufhin er zu arbeiten beginnt. Nach diesem Signal bewirken weitere Eingaben nichts, wodurch das System als Sequenz modelliert werden kann.
Am Anfang wird eine Anzahl an Versuchen festgelegt, die der Roboter beim Aufheben und Hinstellen von Objekten durchführt, bevor die Ausführung als fehlgeschlagen gilt und das System herunterfährt. Dann bewegt sich der Roboter zur Flasche (wie das Glas in der Simulation als Block dargestellt) und hebt diese auf. Dabei ist wichtig, dass nach dem Aufnehmen die Flasche in einem aufrechten Zustand gehalten wird, um ein Verschütten der (in Simulation nicht vorhandenen) Flüssigkeit zu verhindern. Dann bewegt der Roboter die Flasche neben das Glas und beginnt sie in Richtung des Glases zu kippen. Dabei wird die Beschränkung der Orientierung temporär aufgehoben, bis das Wiederaufrichten durchgeführt wurde und die Flasche wieder an ihre ursprüngliche Position gebracht werden kann. Nach dem Abstellen der Flasche wird das Glas angehoben. Der Weg zum Glas unterliegt wieder keinerlei Beschränkungen, während bei angehobenem Glas dieses aufrecht gehalten werden muss, bis es auf der anderen Seite abgestellt wurde, woraufhin sich der Roboterarm wieder in seine Startposition begibt. Beim Abstellen ist ein zusätzlicher Fallback integriert, der dafür sorgt, dass beim Fehlschlagen des Platzierens am Ziel das Glas wieder an seine Ursprungsposition gebracht und neu aufgehoben wird. Dies geschieht wie die einzelnen Versuche auch so oft wie am Anfang festgelegt. Dabei wird im Modell keine Aktualisierung der Orientierungsbeschränkung vorgenommen, da diese nach dem Abstellen zwar entfernt, aber durch das sofortige Wiederaufheben unmittelbar wieder hinzugefügt wird.
Das aus diesen Angaben erstellte Modell ist in \cref{cobot:formales_modell_bild} dargestellt.\\
\begin{figure}[h]
	\centering
	\includegraphics[scale=.18]{./cobot1_v1.png}
	\caption{Formales Modell des Cobots}
	\label{cobot:formales_modell_bild}
\end{figure}
Zu überprüfen ist neben der Reihenfolge der Ausführung auch die Qualität dieser. Zum einen sind die tatsächlichen Positionen von Glas und Flasche relevant, zum anderen müssen die Beschränkungen beim Bewegen der gefüllten Behältnisse abgeglichen werden. 
Die am Anfang der Systemausführung übergebene Anzahl an Versuchen zum Platzieren oder Aufheben ist in diesem Modell zwischen (einschließlich) 5 und 10 festgelegt, kann aber beliebig angepasst werden.
Zeitliche Anforderungen an das System sind nicht gegeben, es ist jedoch wichtig, dass das System terminiert, auch wenn die Ausführung nicht erfolgreich stattfindet und daher Heruntergefahren wird. Aus diesem Grund kann bereits jetzt eine Verifizierung dieses Modells stattfinden.
Der Uppaal Verifier kann mit der Anfrage \textit{A<> deadlock} (jeder Pfad führt zu einem Deadlock (= terminiert)) nicht umgehen. Daher werden 3 Anfragen verwendet, die diesem Zusammenhang entsprechen:
\begin{enumerate}
	\item A[] (deadlock imply (Cobot.finished\_success or Cobot.shut\_down))
	\item A[] ((Cobot.finished\_success or Cobot.shut\_down) imply deadlock)
	\item A<> (Cobot.finished\_success or Cobot.shut\_down)
\end{enumerate}
1. bedeutet dass wenn ein Deadlock auftritt sich der Cobot immer im Zustand \textit{finished\_success} oder \textit{shut\_down} befindet. 2. kehrt diese Aussage um und sorgt so zusammen mit 1. für einen genau-dann-wenn Zusammenhang zwischen einem Deadlock und den besagten Zuständen. Schließlich wird mit 3. abgefragt, ob jeder Pfad (nach endlicher Zeit) zu \textit{finished\_success} oder \textit{shut\_down} führt. Diese Anfragen werden alle zu wahr evaluiert, wodurch klar ist, dass das Modell immer terminiert. Hier ist darauf hinzuweisen, dass für die Validierung ein weiteres Modell im System von Uppaal hinzugefügt werden muss, welches lediglich aus einem Knoten besteht, der sämtliche Channel aktiviert.

\subsection{Topics, Adapter und Modellanpassungen}
Als Nächstes müssen ROS-Topics identifiziert werden, die die benötigten Informationen transportieren. Dazu eignet sich das Tool \textit{rostopic}\footnote{\url{http://wiki.ros.org/rostopic}}, mit welchem sich alle aktiven Topics auflisten und deren Nachrichten abhören lassen.\\
Für den initialen Impuls des Drucksensors ist bereits ein Topic vorhanden und wird vom Cobot verwendet, kann also auch als Output vom Adapter benutzt werden. Dabei lässt sich direkt die Anzahl an erneuten Versuchen vor einem Abbruch übertragen, da diese in einer Konfigurationsdatei des Systems festgelegt wird und daher in der Konfigurationsphase des Adapters ausgelesen werden kann. In dieser Konfigurationsdatei sind auch die Startposition der Flasche sowie die Start- und Zielposition des Glases hinterlegt, sodass auch diese im Adapter gehalten werden können. Das Speichern sowie Vergleichen der Positionen von Flasche und Glas muss im Adapter erfolgen, da Uppaal TRON keine Gleitkommazahlen beim Testen unterstützt. Gazebo veröffentlicht regelmäßig über \textit{/gazebo/link\_states} und \textit{/gazebo/model\_states} die aktuellen Positionen in der Simulationsmodelle, welche folglich mit den Gewünschten verglichen und die Ergebnisse als Differenz (gerundet) an TRON weitergegeben werden können. Anzumerken ist, dass für jede Differenz ein eigener Channel in Uppaal existiert. Dies ist nötig da eine einzelne Kante, welche alle Werte aktualisiert, dazu führen würde, dass Uppaal alle Kombinationen dieser als mögliche Outputs berechnet, was zu exponentiell erhöhter Rechenzeit führt (\textit{State Explosion}). Die Simulationssoftware beginnt vor dem Initialisieren der Objekte bereits mit dem Veröffentlichen von Nachrichten, sodass vom Adapter mit dem Weitergeben an TRON gewartet wird, bis die Objekte initialisiert wurden. Außerdem wird beim Berechnen der Orientierungsabweichung eine Drehung um die z-Achse (senkrecht zur Szene) ignoriert, da diese für ein Aufrechtstehen nicht relevant ist.
Diese Aspekte schränken zwar das Modell ein und machen eine Auslagerung von Logik in den Adapter notwendig, sorgen aber gleichzeitig dafür, dass das Modell abstrakt bleibt und nicht zu detailliert wird.
Um die Positionen der Objekte im Uppaal-System festzuhalten wurde je ein separates Modell, zu sehen in \cref{cobot:positionen_modell_bild} (analog für Flasche) hinzugefügt, welches durch das Topic \textit{/gazebo/link\_states} aktualisiert wird und globale Variablen setzt, die das Modell des Cobots verwendet, um festzustellen, ob sich das Objekt an der richtigen Position befindet. Dabei musste auch darauf geachtet werden, dass fehlgeschlagene Versuche passend dargestellt sind, es existiert also für jeden Zustand eine Kante zu seinem Vorgänger. Zum Startzustand kann allerdings nur zurückgegangen werden, wenn das Objekt nicht gleichzeitig im Endzustand ist, um ein fälschliches Zurückgehen zu verhindern. Es existiert keine Kante direkt vom Start- zum Endzustand, sodass das Objekt mindestens einmal seine Position verlassen muss.\\
\begin{figure}[h]
	\centering
	\includegraphics[scale=.22]{./cobot1_positionen_modell.png}
	\caption{Modell der Positionen vom Glas}
	\label{cobot:positionen_modell_bild}
\end{figure}
Für das Platzieren und Aufheben von Objekten sowie die Bewegungsplanung wird von ROS beziehungsweise MoveIt\footnote{\url{https://moveit.ros.org/}} (Framework unter anderem zur Bewegungsplanung) die actionlib verwendet, sodass sich die dort verwendeten Topics \textit{/pickup/result} und \textit{/place/result} zum Testen eignen. Die dort benutzten Nachrichten besitzen einen eigenen Typ von Fehlercode, durch welchen an TRON der Erfolg oder das Fehlschlagen einer Aktion gemeldet werden kann. Um das Modell eindeutiger zu gestalten, wurde die Variable \textit{success} durch je einen weiteren Channel für einen Fehlschlag ersetzt.\\
Für das Verfolgen von Bewegungen, welche bei der Bewegung zur Flasche, zum Glas und beim Einfüllen relevant sind, wird das Topic \textit{/follow\_joint\_trajectory/result} des \textit{position\_joint\_trajectory\_controller} genutzt. Außerdem ist eine Bewegung nach dem Fehlschlagen des Platzierens vom Glas eingefügt worden, da diese hier hinzugefügt wurde, um Abstand zu gewinnen, bevor ein weiterer Versuch ausgeführt wird.
Die restlichen Bewegungen (bis auf die zur Startposition des Roboters) sind als Teilvorgänge des Platzierens und Aufhebens anzusehen, da sie an sich keine essenzielle Funktion des Cobots darstellen und sind daher nicht explizit im Modell angegeben.
Da logischerweise auch das Platzieren und Aufheben von Objekten Bewegungen des Arms beinhaltet, müssen bei entsprechenden Knoten im Modell zusätzliche Kanten eingefügt werden, die zum Knoten selbst zurückführen und dabei eine (nicht-) erfolgreiche Ausführung durch einen Channel abfragen. Dies ist nötig, da TRON sonst einen Fehler aufgrund von nicht erwartetem Output ausgibt. Diese Kanten sorgen dafür, dass die in \cref{cobot:formales_modell} beschriebene Validierung nicht ohne Weiteres so verwendet werden kann, da nun Endlosschleifen möglich sind. Dies könnte beispielsweise durch einen Zähler mit einer (unrealistisch hohen) maximalen Anzahl an Bewegungen und eine Deklarierung von allen Channels als \textit{urgent} realisiert sein, wird jedoch hier nicht vorgenommen, um das Modell übersichtlicher zu halten.\\
Für die Orientierungsbeschränkung beim Bewegen kommen nur Topics, die ein Ziel festlegen (\textit{/move\_group/goal}, \textit{/pickup/goal}, \textit{/place/goal}) infrage, da nur in diesen Nachrichten eine solche Beschränkung übergeben wird und eine Überprüfung von bereits berechneten Bewegungsbahnen aufwändig wäre. Anzumerken ist hier, dass das Einhalten einer gegebenen Beschränkung genauso wie das Aufheben oder die Bewegungen selbst nicht Teil des Tests darstellen, sondern auf den niedrigeren Testebenen (Unit-Tests oder Integrations-Tests) getestet werden müssen. Daher wird hier nur überprüft, ob eine Orientierungsbeschränkung festgelegt ist. Auch hier wäre ein Abgleich mit einer gewünschten Orientierung innerhalb des Adapters möglich, diese ist jedoch nicht zwingend nötig, da eine falsche Orientierung bei einer Simulation extrem auffällig wäre und daher hier ausgeschlossen werden kann. Bei genauerer Analyse des Cobots sind weitere Bewegungsbeschränkungen wie etwa bei der Beschleunigung aufgefallen, diese würden dem gleichen Schema folgen.\\
Da innerhalb von jedem Modell-Zustand Nachrichten an \textit{goal} gesendet werden können, für das Testen allerdings nur eine Änderung bei der Orientierungsbeschränkung relevant ist, wird in das Uppaal-System ein weiteres Modell integriert, welches Outputs an den entsprechenden Channel entgegennimmt, wenn keine Änderung erfolgte. Das gleiche Prinzip wird ebenfalls auf andere Channel angewandt, welche zu jedem Zeitpunkt aktiviert werden können.\\
Das endgültige Testmodell für den Cobot (ohne die zusätzlich verwendeten Modelle, siehe \cref{git-repos}) ist in \cref{cobot:modell_bild} abgebildet.
\begin{figure}[h]
	\centering
	\includegraphics[scale=.22, angle=90]{./cobot1.png}
	\caption{Modell des Cobot-Verhaltens}
	\label{cobot:modell_bild}
\end{figure}

\subsection{Testausführung}\label{cobot:testausfuehrung}
Zur Ausführung wurde ein simples Bash-Script geschrieben, welches zuerst einen build für das Projekt ausführt und dann eine beliebige Anzahl an Tests startet. Dabei werden die Tests sequentiell ausgeführt, indem zunächst TRON und schließlich eine roslaunch-Datei gestartet wird. In dieser Datei ist das Starten der Simulation und nötigen ros-Nodes, inklusive des Test-Adapters, geregelt. Mittels dem Argument \textit{gui} (und dem Wert \textit{false}) wird Gazebo \textit{headless} gestartet, sodass die Tests problemlos im Hintergrund ablaufen können. TRON ermöglicht das Festhalten der Ergebnisse in Textdateien (siehe \cite{TronManual}). Das Tool bricht eine Testausführung ab, wenn ein Fehler auftritt oder der im Adapter festgelegte Timeout abläuft. Da das System nicht unendlich läuft und ein Erfolg mittels der Zustände \textit{finished\_success} und \textit{shut\_down} kodiert ist, kann ein erfolgreicher Test vor dem Timeout beendet werden. Dazu wurde ein weiterer Channel \textit{test\_done} deklariert, welcher beim Übergang in diese Zustände auslöst und als Input dient, auf welchen der Adapter mit einer von TRON nicht erwarteten Ausgabe reagiert (Channel \textit{intentional\_fail}). Dadurch können bei gleichbleibender Zeit deutlich mehr Tests durchgeführt werden, wenn in Kauf genommen wird, dass nach dem im Modell spezifizierten Verhalten keine weitere Überprüfung stattfindet.

\subsection{Testergebnisse}
Die in \cref{cobot:testausfuehrung} beschriebene Methode wurde angewandt, um das Modell 100 mal auszuführen und Log-Dateien anzulegen. Folglich konnte erneut ein Bash-Skript genutzt werden, um die Testergebnisse zu analysieren. Da lediglich ein Zustand erreicht werden muss, um den Test erfolgreich abzuschließen, reicht es in diesem Fall nach einem entsprechenden Zustand im Log zu suchen. Wird kein Endzustand erreicht, so kann der Testfall als fehlgeschlagen eingestuft und weiter untersucht werden.\\
In 46 der 100 Fälle wurde die Sequenz erfolgreich ausgeführt und die Flasche sowie das Glas waren in der richtigen Position. Dabei wurde eine Toleranz von 5cm verwendet. Nur 3 Fälle führten zum Herunterfahren des Systems, was ebenfalls als erfolgreicher Abschluss gezählt werden kann. Die restlichen 51 Ausführungen führten zu keinem Erfolg und wurden im Folgenden manuell analysiert.\\
Die größte Fehlerquelle stellte das Neigen der Flasche beim Befüllen des Glases dar. Es kam dabei 42 mal zum Umfallen mindestens eines Objektes.
\cref{fallbeispiel:testergebnisse:log} zeigt das Ende des Logs eines fehlgeschlagenen Tests. Der Channel \textit{place\_success} wurde aktiviert während sich das Modell des Cobots im Zustand \textit{placing\_bottle} befindet, obwohl die Flasche 17cm von seiner Zielposition entfernt war und 90 Grad von der aufrechten Position abwich, sie lag also auf dem Boden.
Mithilfe der protokollierten Abstände von Positionen und Orientierungen sowie der Zustände, in denen diese auftraten, lässt sich grob feststellen, wann ein solches Ereignis auftrat. 
In 27 Fällen war die Flasche zu mindestens einem Zeitpunkt mehr als 110 Grad geneigt, sodass wahrscheinlich kurz vor oder bei der vollen Neigung der Flasche diese verloren ging. Davon wurde in 3 Fällen auch das Glas umgeworfen. Die restlichen 15 mal fiel die Flasche vermutlich eher, da hier keine 110 Grad bei der Orientierungsdifferenz erreicht wurde. Einmal war die Flasche dabei mehr als 2 Meter von seiner Startposition entfernt, was bei dem Tempo der Bewegungen des Roboters äußerst unwahrscheinlich wirkt und vielleicht durch einen Bug (in der Simulation) verursacht wurde. \\
Nach dem erfolgreichen Befüllen kam es in zwei Fällen zum Umwerfen des Glases beim Aufheben dieses. Weitere zwei Fälle wurde es kurz angehoben und fiel scheinbar zurück in seine Startposition.\\
Die übrig gebliebenen Testfälle lassen sich unter anderem auf die Planung von Bewegungen zurückführen. So hielt das System je einmal im Zustand \textit{stepping\_back} und \textit{goto\_last\_state\_before\_shutdown}, welche beide auf das Fehlschlagen vom Platzieren des Glases folgen, um den nächsten Platzierungsversuch von einer (leicht) anderen Position auszuführen. Bei genauerer Inspektion des Codes fiel auf, dass an dieser Stelle ein Fehlschlagen der Bewegungsplanung nicht verarbeitet wird, was die Fehlerquelle darstellen könnte.\\
Die letzten 3 Testausführungen sind (vermutlich) vom eigentlichen System unabhängig aufgetretene Fehler. Zwei mal wurde im Zustand \textit{in\_start\_pos} ein Update von Positionen der Simulation empfangen, was für TRON zu spät war, da als Nächstes der Übergang zu \textit{finished\_success} aufgrund des urgent Channels \textit{asap} hätte stattfinden sollen. Da an diesem Punkt aber bereits die Ausführung des Systems beendet war und sich sowohl die Flasche als auch das Glas an den richtigen Positionen befanden, sind diese Fälle als erfolgreiche Systemausführung zu werten und es sollte eine Korrektur auf diese fehlerhafte Modellierung folgen. Beispielsweise könnten die Channel, welche Positionsupdates empfangen, ebenfalls als urgent gekennzeichnet werden.\\
Der letzte Fehler konnte in der Konfiguration des Adapters identifiziert werden. Im Zustand \textit{move\_to\_glass} wurde eine Nachricht ohne Orientierungsbeschränkung empfangen, was nur auftreten konnte, da für den Channel \textit{goal}, welcher diese Beschränkung übermittelt, mehrere Topics zuständig sind, sodass in der Queue des Subscribers für eines dieser Topics noch eine Nachricht vorhanden war, in der keine Beschränkung besteht, während eine andere mit dieser (von einem anderen Topic)  bereits verarbeitet war. Das Auftreten eines solchen Fehlers ist als sehr selten einzustufen und könnte verhindert werden, indem etwa nur die letzte Nachricht der drei Topics an TRON weitergegeben wird.\\
Insgesamt ist festzustellen, dass eine Verbesserung des Befüllvorgangs mit Abstand am relevantesten zur Fehlerreduzierung ist. Aufgrund der hohen Anzahl dieser Fehler ist zu vermuten, dass es Probleme bei der Simulation des Festhaltens gibt.
Außerdem sollte das Fehlschlagen von Planungen der Bewegungen immer beachtet und entsprechend damit umgegangen werden. Schließlich könnte das Aufheben und Platzieren von Objekten zuverlässiger gestaltet werden. Unabhängig vom zu testenden System sind auch Fehler im Modell und Adapter aufgefallen, welche jedoch relativ simpel entfernt werden können. Eine kompakte Auflistung aller Testergebnisse ist in \cref{cobot:testergebnisse:tabelle} zu sehen.

\lstset{
	escapeinside={*}{*}
}
\renewcommand{\lstlistingname}{Log}
\begin{lstlisting}[tabsize=1, language={}, caption={Letzter Status bei einem fehlgeschlagenen Test}, label=fallbeispiel:testergebnisse:log]
	( *\textbf{Cobot.placing\_bottle}* press._id9 move._id10 pickup._id11 place._id0 goal_._id7 goalcatcher._id47 positionupdate._id46 positionupdatecatcher._id45 bottle_position.moving glass_position.start_pos testdonecatcher._id44 asapcatcher._id12 )
	#t>154, #t<=155 retry_count=5 init_retry_count=5 place_glass_retry=0 orient_constraint_set=1 bottle_diff_to_start_pos=17 bottle_diff_to_start_ang=90 *\textbf{bottle\_diff\_to\_target\_pos=17}* *\textbf{bottle\_diff\_to\_target\_ang=90}* glass_diff_to_start_pos=0 glass_diff_to_start_ang=0 glass_diff_to_target_pos=140 glass_diff_to_target_ang=0 bottle_in_start_pos=0 bottle_in_target_pos=0 glass_in_start_pos=1 glass_in_target_pos=0 
	...
	Got unacceptable output: *\textbf{place\_success()}*@154032823us at (154;155)
	TEST FAILED: Observed unacceptable output.
	Time elapsed: 154 tu = 154.082412s
	Time    left: 446 tu = 445.917588s
	Random  seed: 1628691301
\end{lstlisting}

\begin{table}[h]
	\caption{Testergebnisse}
	\label{cobot:testergebnisse:tabelle}
	{\small
\begin{tabularx}{\textwidth}{rlX}
	\toprule[.3mm]
	\textbf{Anzahl} & \textbf{Ergebnis} & \textbf{(potentieller) Fehler} \\
	\midrule[.3mm]
	46 & erfolgreiche Ausführung & - \\
	3 & System heruntergefahren (Erfolg) & - \\
	42 & Flasche (und Glas) gefallen & Probleme beim Aufheben oder Festhalten \\
	4 & Glas gefallen & Probleme beim Aufheben oder Festhalten \\
	2 & System angehalten & fehlgeschlagene Bewegungsplanung \\
	2 & Zu spätes Positionsupdate & Modellfehler \\
	1 & Nicht mehr aktuelle Nachricht & Fehler im Adapter \\
\end{tabularx}
}
\end{table}



